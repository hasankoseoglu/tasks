import styles from '../styles/Home.module.css'
import Content from "./Content";
import Footer from "./Footer";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";


export default function Layout({ children }) {
        return (
                <div className= {styles.container_fluid}>
                        {children}
                        <Navbar></Navbar>
                        <Sidebar></Sidebar>
                        <Content></Content>
                        <Footer></Footer>
                </div>
        )
}
