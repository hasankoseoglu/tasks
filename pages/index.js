import Head from 'next/head'



export default function Home() {
  return (
    <>
      <Head>
        <title>Telekomat Task Manager</title>
        <meta name="description" content="Bildirim Sistemi" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
    </>
  )
}
